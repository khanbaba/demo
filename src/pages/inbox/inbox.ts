import { Component } from '@angular/core';
import { NavController, App, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { ChatPage } from '../chat/chat';
import { ModalController } from 'ionic-angular';
/**
 * Generated class for the InboxPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-inbox',
  templateUrl: 'inbox.html',
})
export class InboxPage {

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController
  ) {
  }


  gotoConversation(index: any) {

    let modal = this.modalCtrl.create(ChatPage, {});

    modal.present();
  }
}

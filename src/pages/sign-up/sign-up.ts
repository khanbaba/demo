import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { TabsPage } from '../tabs/tabs';
/**
 * Generated class for the SignUpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {
  public email: String;
  public name: String;
  public password: String;
  public register: FormGroup;
  public user: any;
  public validForm: any;
  constructor(public navCtrl: NavController,
    private _formBuilder: FormBuilder,
    private toastCtrl: ToastController,
    private storage: Storage,
    public navParams: NavParams) {
    this.validForm = true;
    this.buildForm();
  }

  buildForm() {
    this.register = new FormGroup({
      name: new FormControl('', Validators.compose([Validators.required, Validators.maxLength(40)])),
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl('', Validators.compose([Validators.required, Validators.minLength(5)]))
    });
  }

  registerUser() {
    this.email = this.register.get('email').value;
    this.password = this.register.get('password').value;
    this.name = this.register.get('name').value;
    this.user = {
      email: this.email,
      password: this.password,
      name: this.name
    }

    this.storage.set('user', this.user)
      .then(res => {
        let toast = this.toastCtrl.create({
          message: 'Register successful',
          duration: 3000
        });
        toast.present();
        this.storage.set('provider', 'normal');
        this.navCtrl.push(TabsPage, {}, { animate: false });
      });
  }

}

import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  constructor(private qrScanner: QRScanner, public navCtrl: NavController, public platform: Platform) {
    this.platform.registerBackButtonAction(() => {
      this.backButton();
    })
  }

  backButton() {
    let view = this.navCtrl.getActive();
    if (view.component.name) {
      var elem = document.getElementsByTagName('body')[0];
      elem.classList.remove('density');
      this.qrScanner.hide();
    }

  }
  open() {
    var elem = document.getElementsByTagName('body')[0];
    elem.classList.add('density');
    this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
        if (status.authorized) {
          this.qrScanner.useCamera(0).then(res => {
          })
          // start scanning
          let scanSub = this.qrScanner.scan().subscribe((text: string) => {
            alert(text);
            elem.classList.remove('density');
            this.qrScanner.hide(); // hide camera preview
            scanSub.unsubscribe(); // stop scanning
          });

          // show camera preview

          this.qrScanner.show();

        } else if (status.denied) {

        } else {
        }
      })
      .catch((e: any) => {
      });

  }

}

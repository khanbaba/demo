import { Component } from '@angular/core';
import { NavController, App } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../login/login';
import { TabsPage } from '../tabs/tabs';
import { TextToSpeech } from '@ionic-native/text-to-speech';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  public data: any;
  public provider: any;
  public words: any;
  constructor(public navCtrl: NavController, public storage: Storage, private tts: TextToSpeech, private appCtrl: App) {

  }
  ionViewWillEnter() {
    this.storage.get('provider')
      .then(res => {
        this.provider = res;
      })
      .catch((error) => {
      });
    this.storage.get('user')
      .then(res => {
        this.data = res;
      })
      .catch((error) => {
      });
  }
  text() {
    this.tts.speak(this.words)
      .then(() => {
      })
      .catch((reason: any) => {
      });

  }
  logout() {
    this.storage.clear().then(res => {
      this.appCtrl.getRootNav().setRoot(LoginPage);
    })
  }

}

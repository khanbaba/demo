import { Component, ViewChild } from '@angular/core';
import { IonicPage, App, NavController, ViewController, NavParams, Content, Platform, LoadingController, ToastController } from 'ionic-angular';



// import { InAppBrowser } from '@ionic-native/in-app-browser';

/**
 * Generated class for the ChatPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
	selector: 'page-chat',
	templateUrl: 'chat.html',
})
export class ChatPage {
	@ViewChild(Content) content: Content;

	constructor(public navCtrl: NavController,
		public navParams: NavParams,
		public viewCtrl: ViewController,
	) {

	}

	back() {
		this.viewCtrl.dismiss('data');
	}
}
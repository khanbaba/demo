import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';
import { Storage } from '@ionic/storage';
import { TabsPage } from '../tabs/tabs';
import { SignUpPage } from '../sign-up/sign-up';
import { LinkedIn, LinkedInLoginScopes } from '@ionic-native/linkedin';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  public email: String;
  public password: String;
  public login: FormGroup;
  public validForm: any;
  constructor(public navCtrl: NavController,
    private _formBuilder: FormBuilder,
    private toastCtrl: ToastController,
    private storage: Storage,
    private linkedin: LinkedIn,
    private fb: Facebook,
    private googlePlus: GooglePlus,
    public navParams: NavParams) {
    this.buildForm();
  }
  ionViewWillEnter() {
    this.validForm = true;
  }
  buildForm() {
    this.login = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl('', Validators.compose([Validators.required, Validators.minLength(5)]))
    });
  }

  userLogin() {
    this.email = this.login.get('email').value;
    this.password = this.login.get('password').value;
    var user = { 'email': this.email, 'password': this.password };
    this.storage.set('user', user)
      .then(res => {
        let toast = this.toastCtrl.create({
          message: 'Login successful',
          duration: 3000
        });
        toast.present();
        this.storage.set('provider', 'normal');
        this.navCtrl.push(TabsPage, {}, { animate: false })
      })
  }

  facebook() {
    this.fb.login(['public_profile', 'user_friends', 'email'])
      .then((res: FacebookLoginResponse) => {
        this.storage.set('user', res)
          .then(res => {
            let toast = this.toastCtrl.create({
              message: 'Login successful',
              duration: 3000
            });
            toast.present();
            this.storage.set('provider', 'fb');
            this.navCtrl.push(TabsPage, {}, { animate: false })
          })
      })
      .catch(e => {
      });
  }

  google() {
    this.googlePlus.login({})
      .then(res => {
        this.storage.set('user', res)
          .then(res => {
            let toast = this.toastCtrl.create({
              message: 'Login successful',
              duration: 3000
            });
            toast.present();
            this.storage.set('provider', 'google');
            this.navCtrl.push(TabsPage, {}, { animate: false })
          })
      })
      .catch(err => {
      });
  }

  register() {
    this.navCtrl.push(SignUpPage, {}, { animate: false })
  }

  linkedIn() {
    this.linkedin.hasActiveSession().then((active) => {
      if (active == false) {
        let toast = this.toastCtrl.create({
          message: 'Please install linkedin app first from playstore.',
          duration: 3000
        });
        toast.present();
      }

    });

    // login
    const scopes: LinkedInLoginScopes[] = ['r_basicprofile', 'r_emailaddress', 'rw_company_admin', 'w_share'];
    this.linkedin.login(scopes, true)
      .then(res => {

        this.linkedin.getRequest('people/~')
          .then(res => {
            this.storage.set('user', res).then(res => {
              let toast = this.toastCtrl.create({
                message: 'Login successful',
                duration: 3000
              });
              toast.present();
            })
              .catch(e => {

              });
            this.storage.set('provider', 'linkedin');
            this.navCtrl.push(TabsPage, {}, { animate: false })
          })

      })
      .catch(e => {
      });


  }

}
